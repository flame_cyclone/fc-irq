;==================================================
;Mapper号
MAPPER_NUMBER               =   10
;==================================================
;PRG切页
MAPPER_REG_PRG_8000         =   $A000
;7  bit  0
;---- ----
;xxxx PPPP
;     ||||
;     ++++- 为CPU地址 $8000-$BFFF 选择 16 KB PRG ROM 库


;PRG切页
MAPPER_REG_CHR_FD_0000      =   $B000
;7  bit  0
;---- ----
;xxxC CCCC
;   | ||||
;   +-++++- 选择 4 KB CHR ROM 组用于 PPU $0000-$0FFF，当锁存器 0 = $FD 时使用
MAPPER_REG_CHR_FE_0000      =   $C000
;7  bit  0
;---- ----
;xxxC CCCC
;   | ||||
;   +-++++- 选择 4 KB CHR ROM 组用于 PPU $0000-$0FFF，当锁存器 0 = $FE 时使用
MAPPER_REG_CHR_FD_1000      =   $D000
;7  bit  0
;---- ----
;xxxC CCCC
;   | ||||
;   +-++++- 选择 4 KB CHR ROM 组用于 PPU $1000-$1FFF，当锁存器 0 = $FD 时使用
MAPPER_REG_CHR_FE_1000      =   $E000
;7  bit  0
;---- ----
;xxxC CCCC
;   | ||||
;   +-++++- 选择 4 KB CHR ROM 组用于 PPU $1000-$1FFF，当锁存器 0 = $FE 时使用

MAPPER_REG_MIRRORING        =   $F000
;7  bit  0
;---- ----
;xxxx xxxM
;        |
;        +- 选择命名表镜像 (0: 垂直; 1: 水平)

;==================================================
IRQ_SCANLINE_BEGIN          = 136
IRQ_SCANLINE_1              = 8
IRQ_SCANLINE_2              = 54
IRQ_SCANLINE_3              = 8

;==================================================

;====================================================================================================
;宏常量
;====================================================================================================

;====================================================================================================
MACRO_MAPPER_INIT .MACRO
Init_Mapper
;初始化图形块
 
 LDA #$00
 STA MAPPER_REG_CHR_FD_0000
 LDA #$00
 STA MAPPER_REG_CHR_FE_0000
 LDA #$01
 STA MAPPER_REG_CHR_FD_1000
 LDA #$01
 STA MAPPER_REG_CHR_FE_1000
 
 LDA #$01
 STA MAPPER_REG_MIRRORING
 
 .ENDM

;====================================================================================================
MACRO_MAPPER_SOUND_CLEAR .MACRO
 .ENDM

;====================================================================================================
MACRO_SRAM_ENABLE .MACRO

 .ENDM
 
;====================================================================================================
MACRO_SWITCH_BANK_8000_A .MACRO
 LSR A
 STA MAPPER_REG_PRG_8000
 .ENDM

MACRO_SWITCH_BANK_A000_A .MACRO
 .ENDM
 
MACRO_SWITCH_BANK_C000_A .MACRO
 .ENDM
 
MACRO_SWITCH_BANK_E000_A .MACRO
 .ENDM

;====================================================================================================
;触发第一个IRQ
MACRO_TRIGGER_FIRST_IRQ .MACRO

 CLI
 .ENDM
 
;====================================================================================================
;IRQ启用
MACRO_ENABLE_IRQ  .MACRO

 .ENDM
 
;====================================================================================================
;IRQ禁用
MACRO_DISABLE_IRQ  .MACRO

 .ENDM
 
;====================================================================================================
;IRQ确认
MACRO_ACK_IRQ .MACRO

 .ENDM
 
;====================================================================================================
MACRO_IRQ_OPERATE   .MACRO

;IRQ滚动模式常量
IRQ_SCROLL_MODE_ZERO    =   0       ;不滚动
IRQ_SCROLL_MODE_LEFT    =   1       ;向左滚动
IRQ_SCROLL_MODE_RIGHT   =   2       ;向右滚动

IRQ_Scanline_Data
 .DB IRQ_SCANLINE_1
 .DB IRQ_SCANLINE_2
 .DB IRQ_SCANLINE_3
 .DW 00 ;关闭IRQ

;IRQ滚动控制模式
IRQ_Scanline_Mode
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT

;==================================================
;;IRQ滚动控制
IRQ_Set_Scroll
 LDX IRQ_Process_Index
 LDA IRQ_Scanline_Mode,X
 CMP #IRQ_SCROLL_MODE_LEFT
 BEQ IRQ_Set_Scroll_Left
 CMP #IRQ_SCROLL_MODE_RIGHT
 BEQ IRQ_Set_Scroll_Right
IRQ_Set_Scroll_Zero;不滚动
 LDA #$00
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Left;向左滚动
 LDA Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Right;向右滚动
 LDA #$00
 SEC
 SBC Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
 
;==================================================
;;IRQ滚动控制
IRQ_Set_Ctrl
 MACRO_ACK_IRQ
 LDX IRQ_Process_Index
 LDA IRQ_Scanline_Data,X
 BNE IRQ_Process_Latch
IRQ_Process_Disable;禁用IRQ
 STA IRQ_Process_Index
 MACRO_DISABLE_IRQ
 RTS
IRQ_Process_Latch;设置下次 IRQ 触发扫描线

 MACRO_ENABLE_IRQ
 INC IRQ_Process_Index
 RTS
 
;==================================================
;IRQ处理
IRQ_Process_By_Index
 JSR IRQ_Set_Scroll
 JSR IRQ_Set_Ctrl
IRQ_Process_End
 RTS
 
 .ENDM
 