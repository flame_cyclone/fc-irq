;[FC][Mapper][IRQ]
;FlameCyclone 20231201

 .INCLUDE "[FC][Mapper][MMC3].asm"
 ;.INCLUDE "[FC][Mapper][MMC5].asm"
 ;.INCLUDE "[FC][Mapper][Namco163].asm"
 ;.INCLUDE "[FC][Mapper][VRC2&4].asm"
 ;.INCLUDE "[FC][Mapper][VRC3].asm"
 ;.INCLUDE "[FC][Mapper][VRC6].asm"
 ;.INCLUDE "[FC][Mapper][VRC7].asm"
 ;.INCLUDE "[FC][Mapper][FME7].asm"
 ;.INCLUDE "[FC][Mapper][Mapper18].asm"
 ;.INCLUDE "[FC][Mapper][Mapper64].asm"
 
;文件头配置
NES_16KB_PRG_SIZE           =   2
NES_8KB_CHR_SIZE            =   1
BANK_DATA_MASK              =   NES_16KB_PRG_SIZE * 2 - 1     ;bank号掩码
RESET_BANK                  =   NES_16KB_PRG_SIZE * 2 - 1
;======================================================================
PRG_DATA_BANK_C000          =   NES_16KB_PRG_SIZE * 2 - 2
PRG_DATA_BANK_E000          =   NES_16KB_PRG_SIZE * 2 - 1

;文件头
;======================================================================
 .INESPRG NES_16KB_PRG_SIZE ;16KB PRG 数量
 .INESCHR NES_8KB_CHR_SIZE  ;8KB CHR 数量
 .INESMAP MAPPER_NUMBER
 .INESMIR 0     ;命名表镜像 0水平 1垂直

;==================================================
;NES端口常量
PPU_CTRL                    =   $2000   ;PPU控制寄存器
PPU_MASK                    =   $2001   ;PPU掩码寄存器
PPU_STATUS                  =   $2002   ;PPU状态寄存器：读取后PPU_SCROLL和PPU_ADDRESS被复位，下一个写到PPU_SCROLL的数据是水平的，写到PPU_ADDRESS的数据是高位
PPU_OAM_ADDR                =   $2003   ;精灵RAM地址：用来设置通过PPU_OAM_DATA访问的256字节精灵RAM地址。每次访问PPU_OAM_DATA后该地址增加1
PPU_OAM_DATA                =   $2004   ;精灵RAM数据：用来读/写精灵内存。地址通过PPU_OAM_ADDR来设置，每次访问后地址增加1
PPU_SCROLL                  =   $2005   ;屏幕滚动偏移：第一个写的值会进入垂直滚动寄存器（若>239，被忽略）。第二个值出现在水平滚动寄存器 
PPU_ADDRESS                 =   $2006   ;VRAM地址：设置PPU_DATA访问的VRAM地址。第一个写地址的高6位。第二个写低8位。每次访问PPU_DATA后地址增加
PPU_DATA                    =   $2007   ;VRAM数据：用来访问VRAM数据，通过PPU_ADDRESS设置的地址在每次访问之后会增加1或32 
OAM_DMA                     =   $4014   ;DMA访问精灵RAM：通过写一个值xx到这个端口，引起CPU内存地址为$xx00－$xxFF的区域传送到精灵内存
APU_STATUS                  =   $4015   ;声音通道切换
JOY1_FRAME                  =   $4016   ;手柄1 + 选通
JOY2_FRAME                  =   $4017   ;手柄2 + 选通

;--------------------------------------------------
PROGRAM_BANK                =   PRG_DATA_BANK_E000
PROGRAM_ADDR                =   $E000

;==================================================
;零页内存地址配置
Use_Ram_Begin               =   $80
 .RSSET Use_Ram_Begin
PPU_Ctrl_Buf                .RS 1
PPU_Msak_Buf                .RS 1
PPU_Scroll_H                .RS 1
PPU_Scroll_V                .RS 1
FC_Data_L                   .RS 1
FC_Data_H                   .RS 1
FC_Data_Buf                 .RS 1
FC_Data_Index               .RS 1

;==================================================
GAMEPAD_MERGE_FLAG          =   $04
    
Gamepad_Keep                .RS 2
Gamepad_Once                .RS 2
Gamepad_Temp                .RS 2   
Gamepad_0_State             .RS 1
Gamepad_1_State             .RS 1
Gamepad_0_Value             .RS 1
Gamepad_1_Value             .RS 1
Gamepad_Port_Value          .RS 1
Gamepad_Merge               .RS 1

;==================================================
Scroll_H                    .RS 1
Scroll_V                    .RS 1
IRQ_Process_Index           .RS 1

;==================================================
Prg_Bank_8000               .RS 1
Prg_Bank_A000               .RS 1
Prg_Bank_C000               .RS 1
Prg_Bank_E000               .RS 1
Prg_Bank_8000_Bak           .RS 1
Prg_Bank_A000_Bak           .RS 1
Prg_Bank_C000_Bak           .RS 1
Prg_Bank_E000_Bak           .RS 1

;==================================================
Prg_Bank_A_Bak              .RS 1

;==================================================
