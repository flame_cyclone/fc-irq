;==================================================
;Mapper号
MAPPER_NUMBER               =   64
;==================================================
;Mapper 18 寄存器常量
MAPPER_REG_BANK_SELECT      =   $8000
;7  bit  0
;---- ----
;CPKx RRRR
;|||  ||||
;|||  ++++- Specify which bank register to update on next write to Bank Data register
;|||          0000: R0: Select 2 (K=0) or 1 (K=1) KiB CHR bank at PPU $0000 (or $1000)
;|||          0001: R1: Select 2 (K=0) or 1 (K=1) KiB CHR bank at PPU $0800 (or $1800)
;|||          0010: R2: Select 1 KiB CHR bank at PPU $1000-$13FF (or $0000-$03FF)
;|||          0011: R3: Select 1 KiB CHR bank at PPU $1400-$17FF (or $0400-$07FF)
;|||          0100: R4: Select 1 KiB CHR bank at PPU $1800-$1BFF (or $0800-$0BFF)
;|||          0101: R5: Select 1 KiB CHR bank at PPU $1C00-$1FFF (or $0C00-$0FFF)
;|||          0110: R6: Select 8 KiB PRG ROM bank at $8000-$9FFF (or $C000-$DFFF)
;|||          0111: R7: Select 8 KiB PRG ROM bank at $A000-$BFFF
;|||          1000: R8: If K=1, Select 1 KiB CHR bank at PPU $0400 (or $1400)
;|||          1001: R9: If K=1, Select 1 KiB CHR bank at PPU $0C00 (or $1C00)
;|||          1111: RF: Select 8 KiB PRG ROM bank at $C000-$DFFF (or $8000-$9FFF)
;||+------- Full 1 KiB CHR bank mode  0: two 2 KiB banks at $0000-$0FFF (or $1000-$1FFF)
;||                                   1: four 1 KiB banks at $0000-$0FFF (or $1000-$1FFF)
;|+-------- PRG ROM bank mode  0: $8000-$9FFF uses bank selected with R6
;|                                $A000-$BFFF uses bank selected with R7
;|                                $C000-$DFFF uses bank selected with RF
;|                             1: $8000-$9FFF uses bank selected with RF
;|                                $A000-$BFFF uses bank selected with R7
;|                                $C000-$DFFF uses bank selected with R6
;+--------- CHR A12 inversion  0: two 2 KiB banks (or four 1 KiB banks) at $0000-$0FFF
;                                 four 1 KiB banks at $1000-$1FFF
;                              1: two 2 KiB banks (or four 1 KiB banks) at $1000-$1FFF
;                                 four 1 KiB banks at $0000-$0FFF

MAPPER_REG_BANK_DATA        =   $8001
MAPPER_REG_MIRRORING        =   $A000

MAPPER_REG_IRQ_LATCH        =   $C000
MAPPER_REG_IRQ_MODE_RELOAD  =   $C001
MAPPER_REG_IRQ_ACKNOWLEDGE  =   $E000
MAPPER_REG_IRQ_ENABLE       =   $E001
;7  bit  0
;---- ----
;xxxx xxxM
;        |
;        +- Mirroring (0: vertical; 1: horizontal)

;==================================================
IRQ_SCANLINE_BEGIN          =   135
IRQ_SCANLINE_1              =   8
IRQ_SCANLINE_2              =   54
IRQ_SCANLINE_3              =   8
;==================================================

;====================================================================================================
;宏常量
;====================================================================================================

;====================================================================================================
MACRO_MAPPER_INIT .MACRO

 LDA #$01
 STA MAPPER_REG_MIRRORING
 
 LDX #$07
.Init_Chr_Bank
 LDA .ChrBankSelect,X
 STA MAPPER_REG_BANK_SELECT
 LDA .ChrBankData,X
 STA MAPPER_REG_BANK_DATA
 DEX
 BPL .Init_Chr_Bank
 
 JMP .Init_Chr_Bank_End
.ChrBankSelect
 ;.DB $82,$83,$84,$85,$80,$88,$81,$89
 .DB $00,$08,$01,$09,$02,$03,$04,$05
.ChrBankData
 ;.DB $04,$05,$06,$07,$00,$01,$02,$03
 .DB $00,$01,$02,$03,$04,$05,$06,$07

.Init_Chr_Bank_End

 .ENDM

;====================================================================================================
MACRO_MAPPER_SOUND_CLEAR .MACRO
 .ENDM
 
;====================================================================================================
MACRO_SRAM_ENABLE .MACRO

 .ENDM
 
;====================================================================================================
MACRO_SWITCH_BANK_8000_A .MACRO
 PHA
 LDA #%00100110
 STA MAPPER_REG_BANK_SELECT
 PLA
 STA MAPPER_REG_BANK_DATA
 .ENDM

MACRO_SWITCH_BANK_A000_A .MACRO
 PHA
 LDA #%00100111
 STA MAPPER_REG_BANK_SELECT
 PLA
 STA MAPPER_REG_BANK_DATA
 .ENDM
 
MACRO_SWITCH_BANK_C000_A .MACRO
 PHA
 LDA #%00101111
 STA MAPPER_REG_BANK_SELECT
 PLA
 STA MAPPER_REG_BANK_DATA
 .ENDM

MACRO_SWITCH_BANK_E000_A .MACRO
 .ENDM

;====================================================================================================
MACRO_TRIGGER_FIRST_IRQ .MACRO
 LDA #IRQ_SCANLINE_BEGIN
 STA MAPPER_REG_IRQ_LATCH
 MACRO_ENABLE_IRQ
 CLI
 .ENDM
 
;====================================================================================================
MACRO_ENABLE_IRQ  .MACRO
 LDA #$00
 STA MAPPER_REG_IRQ_MODE_RELOAD
 STA MAPPER_REG_IRQ_ENABLE
 .ENDM
 
;====================================================================================================
MACRO_DISABLE_IRQ  .MACRO
 STA MAPPER_REG_IRQ_ACKNOWLEDGE
 .ENDM
 
;====================================================================================================
MACRO_ACK_IRQ .MACRO
 STA MAPPER_REG_IRQ_ACKNOWLEDGE
 .ENDM
 
;====================================================================================================
MACRO_IRQ_OPERATE   .MACRO

;==================================================
;IRQ滚动模式常量
IRQ_SCROLL_MODE_ZERO    =   0       ;不滚动
IRQ_SCROLL_MODE_LEFT    =   1       ;向左滚动
IRQ_SCROLL_MODE_RIGHT   =   2       ;向右滚动

;IRQ扫描线数据
IRQ_Scanline_Data
 .DB IRQ_SCANLINE_1
 .DB IRQ_SCANLINE_2
 .DB IRQ_SCANLINE_3
 .DW 00 ;关闭IRQ

;IRQ滚动控制模式
IRQ_Scanline_Mode
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT

;==================================================
;;IRQ滚动控制
IRQ_Set_Scroll
 LDX IRQ_Process_Index
 LDA IRQ_Scanline_Mode,X
 CMP #IRQ_SCROLL_MODE_LEFT
 BEQ IRQ_Set_Scroll_Left
 CMP #IRQ_SCROLL_MODE_RIGHT
 BEQ IRQ_Set_Scroll_Right
IRQ_Set_Scroll_Zero;不滚动
 LDA #$00
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Left;向左滚动
 LDA Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Right;向右滚动
 LDA #$00
 SEC
 SBC Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
 
;==================================================
;;IRQ滚动控制
IRQ_Set_Ctrl
 LDX IRQ_Process_Index
 LDA IRQ_Scanline_Data,X
 BNE IRQ_Process_Latch
IRQ_Process_Disable;禁用IRQ
 STA IRQ_Process_Index
 MACRO_DISABLE_IRQ
 RTS
IRQ_Process_Latch;设置下次 IRQ 触发扫描线
 STA MAPPER_REG_IRQ_LATCH
 MACRO_ENABLE_IRQ
 INC IRQ_Process_Index
 RTS

;==================================================
;IRQ处理
IRQ_Process_By_Index
 ;MACRO_ACK_IRQ
 JSR IRQ_Set_Scroll
 JSR IRQ_Set_Ctrl
IRQ_Process_End
 RTS
 
 .ENDM
 