;==================================================
;Mapper号
MAPPER_NUMBER               =   18
;==================================================
;Mapper 18 寄存器常量
MAPPER_REG_PRG_8000_L       =   $8000
MAPPER_REG_PRG_8000_H       =   $8001
MAPPER_REG_PRG_A000_L       =   $8002
MAPPER_REG_PRG_A000_H       =   $8003
MAPPER_REG_PRG_C000_L       =   $9000
MAPPER_REG_PRG_C000_H       =   $9001

MAPPER_REG_RAM_PROTECT      =   $9002

MAPPER_REG_CHR_0000_L       =   $A000
MAPPER_REG_CHR_0000_H       =   $A001
MAPPER_REG_CHR_0400_L       =   $A002
MAPPER_REG_CHR_0400_H       =   $A003

MAPPER_REG_CHR_0800_L       =   $B000
MAPPER_REG_CHR_0800_H       =   $B001
MAPPER_REG_CHR_0C00_L       =   $B002
MAPPER_REG_CHR_0C00_H       =   $B003


MAPPER_REG_CHR_1000_L       =   $C000
MAPPER_REG_CHR_1000_H       =   $C001
MAPPER_REG_CHR_1400_L       =   $C002
MAPPER_REG_CHR_1400_H       =   $C003

MAPPER_REG_CHR_1800_L       =   $D000
MAPPER_REG_CHR_1800_H       =   $D001
MAPPER_REG_CHR_1C00_L       =   $D002
MAPPER_REG_CHR_1C00_H       =   $D003

MAPPER_REG_IRQ_VALUE        =   $E000
MAPPER_REG_IRQ_RELOAD       =   $F000
MAPPER_REG_IRQ_CTRL         =   $F001

MAPPER_REG_MIRRORING        =   $F002
MAPPER_REG_SOUND            =   $F003
;==================================================
IRQ_SCANLINE_BEGIN          =   136
IRQ_SCANLINE_1              =   7
IRQ_SCANLINE_2              =   54
IRQ_SCANLINE_3              =   8
;==================================================
BEGIN_LINE_CYCLES_START     =   ((260 - 240 + IRQ_SCANLINE_BEGIN) * 341 / 3)
BEGIN_LINE_CYCLES_0         =   (IRQ_SCANLINE_1 * 341 / 3)
BEGIN_LINE_CYCLES_1         =   (IRQ_SCANLINE_2 * 341 / 3)
BEGIN_LINE_CYCLES_2         =   (IRQ_SCANLINE_3 * 341 / 3)

;====================================================================================================
;宏常量
;====================================================================================================

;====================================================================================================
MACRO_MAPPER_INIT .MACRO
 LDA #$00
 STA MAPPER_REG_CHR_0000_L
 LDA #$01
 STA MAPPER_REG_CHR_0400_L
 LDA #$02
 STA MAPPER_REG_CHR_0800_L
 LDA #$03
 STA MAPPER_REG_CHR_0C00_L
 LDA #$04
 STA MAPPER_REG_CHR_1000_L
 LDA #$05
 STA MAPPER_REG_CHR_1400_L
 LDA #$06
 STA MAPPER_REG_CHR_1800_L
 LDA #$07
 STA MAPPER_REG_CHR_1C00_L
 LDA #$00
 STA MAPPER_REG_CHR_0000_H
 STA MAPPER_REG_CHR_0400_H
 STA MAPPER_REG_CHR_0800_H
 STA MAPPER_REG_CHR_0C00_H
 STA MAPPER_REG_CHR_1000_H
 STA MAPPER_REG_CHR_1400_H
 STA MAPPER_REG_CHR_1800_H
 STA MAPPER_REG_CHR_1C00_H

 LDA #$00
 STA MAPPER_REG_MIRRORING
 
 LDA #$03
 STA MAPPER_REG_RAM_PROTECT

 .ENDM

;====================================================================================================
MACRO_MAPPER_SOUND_CLEAR .MACRO
 .ENDM
 
;====================================================================================================
MACRO_SRAM_ENABLE .MACRO

 .ENDM
 
;====================================================================================================
MACRO_SWITCH_BANK_8000_A .MACRO
 PHA
 STA MAPPER_REG_PRG_8000_L
 LSR A
 LSR A
 LSR A
 LSR A
 STA MAPPER_REG_PRG_8000_H
 PLA
 .ENDM

MACRO_SWITCH_BANK_A000_A .MACRO
 PHA
 STA MAPPER_REG_PRG_A000_L
 LSR A
 LSR A
 LSR A
 LSR A
 STA MAPPER_REG_PRG_A000_H
 PLA
 .ENDM
 
MACRO_SWITCH_BANK_C000_A .MACRO
 PHA
 STA MAPPER_REG_PRG_C000_L
 LSR A
 LSR A
 LSR A
 LSR A
 STA MAPPER_REG_PRG_C000_H
 PLA
 .ENDM

MACRO_SWITCH_BANK_E000_A .MACRO
 .ENDM

;====================================================================================================
MACRO_TRIGGER_FIRST_IRQ .MACRO
 LDA #LOW(BEGIN_LINE_CYCLES_START)
 AND #$0F
 STA MAPPER_REG_IRQ_VALUE + 0
 LDA #LOW(BEGIN_LINE_CYCLES_START)
 LSR A
 LSR A
 LSR A
 LSR A
 STA MAPPER_REG_IRQ_VALUE + 1
 LDA #HIGH(BEGIN_LINE_CYCLES_START)
 AND #$0F
 STA MAPPER_REG_IRQ_VALUE + 2
 LDA #HIGH(BEGIN_LINE_CYCLES_START)
 LSR A
 LSR A
 LSR A
 LSR A
 STA MAPPER_REG_IRQ_VALUE + 3
 LDA #$01
 STA MAPPER_REG_IRQ_CTRL
 STA MAPPER_REG_IRQ_RELOAD
 CLI
 .ENDM
 
;====================================================================================================
MACRO_ENABLE_IRQ  .MACRO
 LDA #$01
 STA MAPPER_REG_IRQ_CTRL
 .ENDM
 
;====================================================================================================
MACRO_DISABLE_IRQ  .MACRO
 LDA #$00
 STA MAPPER_REG_IRQ_CTRL
 .ENDM
 
;====================================================================================================
MACRO_ACK_IRQ .MACRO
 LDA #$00
 STA MAPPER_REG_IRQ_CTRL
 .ENDM
 
;====================================================================================================
MACRO_IRQ_OPERATE   .MACRO

;==================================================
;IRQ滚动模式常量
IRQ_SCROLL_MODE_ZERO    =   0       ;不滚动
IRQ_SCROLL_MODE_LEFT    =   1       ;向左滚动
IRQ_SCROLL_MODE_RIGHT   =   2       ;向右滚动

IRQ_Scanline_Data
 .DW BEGIN_LINE_CYCLES_0
 .DW BEGIN_LINE_CYCLES_1
 .DW BEGIN_LINE_CYCLES_2
 .DW 00 ;关闭IRQ

;IRQ扫描线数据
IRQ_Scanline_Data_L
 .DB LOW(BEGIN_LINE_CYCLES_0) & $0F, LOW(BEGIN_LINE_CYCLES_0) >> 4
 .DB LOW(BEGIN_LINE_CYCLES_1) & $0F, LOW(BEGIN_LINE_CYCLES_1) >> 4
 .DB LOW(BEGIN_LINE_CYCLES_2) & $0F, LOW(BEGIN_LINE_CYCLES_2) >> 4

IRQ_Scanline_Data_H
 .DB HIGH(BEGIN_LINE_CYCLES_0) & $0F, HIGH(BEGIN_LINE_CYCLES_0) >> 4
 .DB HIGH(BEGIN_LINE_CYCLES_1) & $0F, HIGH(BEGIN_LINE_CYCLES_1) >> 4
 .DB HIGH(BEGIN_LINE_CYCLES_2) & $0F, HIGH(BEGIN_LINE_CYCLES_2) >> 4
 
;IRQ滚动控制模式
IRQ_Scanline_Mode
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT
 .DB IRQ_SCROLL_MODE_RIGHT
 .DB IRQ_SCROLL_MODE_LEFT

;==================================================
;;IRQ滚动控制
IRQ_Set_Scroll
 LDX IRQ_Process_Index
 LDA IRQ_Scanline_Mode,X
 CMP #IRQ_SCROLL_MODE_LEFT
 BEQ IRQ_Set_Scroll_Left
 CMP #IRQ_SCROLL_MODE_RIGHT
 BEQ IRQ_Set_Scroll_Right
IRQ_Set_Scroll_Zero;不滚动
 LDA #$00
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Left;向左滚动
 LDA Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
IRQ_Set_Scroll_Right;向右滚动
 LDA #$00
 SEC
 SBC Scroll_H
 STA PPU_SCROLL
 STA PPU_SCROLL
 RTS
 
;==================================================
;;IRQ滚动控制
IRQ_Set_Ctrl
 LDA IRQ_Process_Index
 ASL A
 TAX
 LDA IRQ_Scanline_Data,X
 ORA IRQ_Scanline_Data + 1,X
 BNE IRQ_Process_Latch
IRQ_Process_Disable;禁用IRQ
 STA IRQ_Process_Index
 RTS
IRQ_Process_Latch;设置下次 IRQ 触发扫描线
 LDA IRQ_Scanline_Data_L + 0,X
 STA MAPPER_REG_IRQ_VALUE + 0
 LDA IRQ_Scanline_Data_L + 1,X
 STA MAPPER_REG_IRQ_VALUE + 1
 
 LDA IRQ_Scanline_Data_H + 0,X
 STA MAPPER_REG_IRQ_VALUE + 2
 LDA IRQ_Scanline_Data_H + 1,X
 STA MAPPER_REG_IRQ_VALUE + 3
 
 LDA #$01
 STA MAPPER_REG_IRQ_CTRL
 STA MAPPER_REG_IRQ_RELOAD
 INC IRQ_Process_Index
 RTS

;==================================================
;IRQ处理
IRQ_Process_By_Index
 NOP
 NOP
 NOP
 JSR IRQ_Set_Scroll
 JSR IRQ_Set_Ctrl
IRQ_Process_End
 RTS

 .ENDM
 